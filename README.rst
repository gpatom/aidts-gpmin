===================================================================
gpatom/aidts-gpmin: Tools for atomistic simulations based on Gaussian processes
===================================================================

gpatom/aidts-gpmin is a Python package which provides several tools for
geometry optimisation and related tasks in atomistic systems using machine learning surrogate models.
gpatom/aidts-gpmin is an extension to the `Atomic Simulation Environment <https://wiki.fysik.dtu.dk/ase/>`_.

gpatom/aidts-gpmin contains the following tools:

 * GPMin: An atomistic optimization algorithm based on Gaussian processes.
 * AIDMin: Energy structure minimization through the Artificial-Intelligence framework.
 * AIDNEB: Nudged Elastic Band calculations through the Artificial-Intelligence framework.
 * AIDTS: Transition State Search through the Artificial-Intelligence framework.
 * AIDMEP: Minimum Energy Pathway through the Artificial-Intelligence framework.

Contact
=======

Please join our
`#gpatom <https://app.element.io/#/room/#gpatom:matrix.org>`_
channel on Matrix.


Installation cheat sheet
========================

To install latest release from pypi, use::

  pip install aidts-gpmin

To install a developer version (allows in-place edits of the code), 
clone the sourcecode and go to the toplevel gpatom directory, then run::

  pip install --editable .


Testing cheat sheet
===================

To run the tests, go to the test directory and run::

  $ pytest

Run the tests in parallel on ``n`` cores (requires pytest-xdist)::

  $ pytest -n 4

Show tests without running them::

  $ pytest --collectonly

Run tests in particular module::

  $ pytest test_module.py

Run tests matching pattern::

  $ pytest -k <pattern>

Show output from tests::

  $ pytest -s

Note that since many tests write files, temporary directories are
created for each test.  The temporary directories are located in
``/tmp/pytest-of-<username>/``.  pytest takes care of cleaning up
these test directories.

Use pytest.ini and test/conftest.py to customize how the tests run.
