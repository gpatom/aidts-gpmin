#!/usr/bin/env python3

from setuptools import setup, find_packages


install_requires = [
    'ase>=3.22.0',
    'scipy>=1.11.1',
]


setup(
    name='aidts-gpmin',
    description='Atomistic simulation tools based on Gaussian processes',
    url='https://gitlab.com/gpatom/aidts-gpmin',
    version='0.1.1',
    license='LGPLv2.1+',
    packages=find_packages(),
    install_requires=install_requires,
)
